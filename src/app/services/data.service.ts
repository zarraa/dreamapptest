import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
  }

  async getIP(): Promise<string> {
    try {
      const response: any = await this.http.get(`https://api.db-ip.com/v2/free/self`).toPromise();
      return  response;
    } catch (error) {
      return 'Error';
    }
  }
}
