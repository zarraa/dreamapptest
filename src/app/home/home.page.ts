import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular'; // Importation de Platform pour détecter le type d'appareil
import { FormBuilder, FormGroup, Validators } from '@angular/forms'; // Importation de FormBuilder et FormGroup pour gérer le formulaire
import { AlertController } from '@ionic/angular';
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  isWeb: boolean;
  isMobile: boolean;
  dataForm: FormGroup; // Déclaration d'un objet FormGroup pour le formulaire

  constructor(private platform: Platform, // Injection du service Platform
              private _formBuilder: FormBuilder, // Injection de FormBuilder pour créer le formulaire
              private dataService: DataService, // Injection du service DataService
              private alertController: AlertController) { // Injection du contrôleur d'alerte

    // Utilisation de Platform pour détecter le type d'appareil
    this.isWeb = this.platform.is('desktop');
    this.isMobile = this.platform.is('mobile');

    // Création du formulaire avec FormBuilder
    this.dataForm = this._formBuilder.group({
      name: [null, [Validators.required]], // Champ nom avec validation requise
      intention: [null, Validators.required], // Champ intention avec validation requise
    });
  }

  ngOnInit() {
    // ngOnInit est appelé lorsque le composant est initialisé, vous pouvez ajouter des opérations ici si nécessaire
  }

  async valider() {
    // Fonction de validation appelée lorsque le bouton est cliqué
    if (this.isWeb) {
      if (this.dataForm.valid)
        this.checkIp(); // Appel de la fonction pour vérifier l'IP
    } else
      this.checkIp();
  }

  async checkIp() {
    // Fonction pour vérifier l'IP
    const result: any = await this.dataService.getIP(); // Appel du service DataService pour obtenir l'adresse IP
    const sum = result?.ipAddress.split('.').reduce((acc: any, num: any) => acc + parseInt(num), 0); // Calcul de la somme des parties de l'adresse IP

    let alertMessage = '';
    if (sum > 100) {
      alertMessage = 'OK';
    } else {
      alertMessage = 'KO';
    }

    // Création et affichage de l'alerte avec le contrôleur d'alerte
    const alert = await this.alertController.create({
      header: 'IP Check',
      message: alertMessage,
      buttons: ['OK']
    });

    await alert.present();
  }
}
