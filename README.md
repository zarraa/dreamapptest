
#Dream App - Starting page UI

## Description
Ceci est une application Ionic 6 construite avec Capacitor qui contient une page de bienvenue en mode responsive (UI compatible avec PC et mobile) pour l’application Dream App.

## Prérequis
- Node.js et npm installés sur votre machine.
- Un émulateur Android/iOS ou un appareil physique pour tester l'application.

## Installation
1. Clonez ce dépôt sur votre machine :
git clone https://gitlab.com/zarraa/dreamapptest/
cd votre-projet

2. Installez les dépendances :
npm i

3. Configurez Capacitor pour la plate-forme de votre choix :
npx cap add android or npx cap add ios
                       
4. Synchronisez Capacitor avec votre projet :
## Lancer l'Application

### Sur un Émulateur Android/iOS
1. Assurez-vous que l'émulateur est configuré et fonctionne correctement.

2. Dans votre terminal, exécutez :
npx cap open android

Cela ouvrira votre projet dans Android Studio. Dans l'IDE, vous pouvez sélectionner l'émulateur et lancer l'application.

###Note:
Pour ce point : "Le formulaire doit être valide lors du clic sur le bouton en mode PC", 
j'ai intégré [formGroup]="dataForm" dans la balise <form>, mais cela génère une erreur : "error NG8002: Can't bind to 'formGroup' since it isn't a known property of 'form'".
 J'ai importé ReactiveFormsModule et FormsModule dans le module, mais cela ne fonctionne pas.

N'hésitez pas à me poser des questions si vous avez besoin des explications !



